#!/usr/bin/make -f

DEB_HOST_ARCH_OS ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_OS 2>/dev/null)

DEB_BUILD_MAINT_OPTIONS=hardening=+all
DEB_LDFLAGS_MAINT_APPEND=-Wl,-z,defs -Wl,-O1 -Wl,--as-needed
include /usr/share/dpkg/buildflags.mk

DEB_CONFIGURE_EXTRA_FLAGS := --prefix= \
	                     --sysconfdir=/etc/nut \
	                     --includedir=/usr/include \
			     --mandir=/usr/share/man \
	                     --libdir=\$${prefix}/lib/$(DEB_HOST_MULTIARCH) \
			     --libexecdir=/usr/libexec \
	                     --with-ssl --with-nss \
	                     --with-cgi \
	                     --with-dev \
	                     --enable-static \
	                     --with-statepath=/run/nut \
	                     --with-altpidpath=/run/nut \
	                     --with-drvpath=/lib/nut \
	                     --with-cgipath=/usr/lib/cgi-bin/nut \
	                     --with-htmlpath=/usr/share/nut/www \
	                     --with-pidpath=/run/nut \
	                     --datadir=/usr/share/nut \
	                     --with-pkgconfig-dir=/usr/lib/$(DEB_HOST_MULTIARCH)/pkgconfig \
	                     --with-user=nut --with-group=nut \
	                     --with-udev-dir=/lib/udev \
			     --with-systemdsystemunitdir=/lib/systemd/system \
	                     --with-systemdshutdowndir=/lib/systemd/system-shutdown \
			     --with-systemdtmpfilesdir=/usr/lib/tmpfiles.d


# only build documentation if asciidoc is installed
ifneq ($(shell which asciidoc),)
  DEB_CONFIGURE_EXTRA_FLAGS+=--with-doc=man,html-single,html-chunked,pdf
endif

%:
	dh $@ --with python3

override_dh_auto_configure:
	dh_auto_configure -- $(DEB_CONFIGURE_EXTRA_FLAGS)

override_dh_autoreconf:
	dh_autoreconf --as-needed

override_dh_auto_install-arch:
	dh_auto_install

	# install the bash completion script
	mkdir -p $(CURDIR)/debian/tmp/usr/share/bash-completion/completions/
	cp $(CURDIR)/scripts/misc/nut.bash_completion \
		$(CURDIR)/debian/tmp/usr/share/bash-completion/completions/nut

	# install the avahi service file
	mkdir -p $(CURDIR)/debian/tmp/etc/avahi/services
	cp $(CURDIR)/scripts/avahi/nut.service \
		$(CURDIR)/debian/tmp/etc/avahi/services

	# install Augeas lenses
	mkdir -p $(CURDIR)/debian/tmp/usr/share/augeas/lenses/tests
	cp $(CURDIR)/scripts/augeas/*.aug \
		$(CURDIR)/debian/tmp/usr/share/augeas/lenses/
	cp $(CURDIR)/scripts/augeas/tests/test_nut.aug \
		$(CURDIR)/debian/tmp/usr/share/augeas/lenses/tests/

	# install the default configuration
	for f in $(CURDIR)/debian/tmp/etc/nut/*; do           \
		nf=`basename $${f} | sed 's/\(.*\).sample/\1/'`;  \
		mv $${f} $(CURDIR)/debian/tmp/etc/nut/$${nf};  \
	done

	# Move -dev file to /usr
	mv $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libupsclient.a \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libupsclient.a
	rm -f $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libupsclient.so
	ln -s /lib/$(DEB_HOST_MULTIARCH)/libupsclient.so.6 \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libupsclient.so
	mv $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libnutclient.a \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libnutclient.a
	rm -f $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libnutclient.so
	ln -s /lib/$(DEB_HOST_MULTIARCH)/libnutclient.so.2 \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libnutclient.so
	mv $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libnutclientstub.a \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libnutclientstub.a
	rm -f $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libnutclientstub.so
	ln -s /lib/$(DEB_HOST_MULTIARCH)/libnutclientstub.so.1 \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libnutclientstub.so

	rm -f $(CURDIR)/debian/tmp/lib/$(DEB_HOST_MULTIARCH)/libnutscan.so
	ln -s /lib/$(DEB_HOST_MULTIARCH)/libnutscan.so.2 \
		$(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libnutscan.so

	# Move daemons and install wrappers
	mv $(CURDIR)/debian/tmp/sbin/upsmon $(CURDIR)/debian/tmp/lib/nut/upsmon
	mv $(CURDIR)/debian/tmp/sbin/upsd $(CURDIR)/debian/tmp/lib/nut/upsd
	install -m 0755 debian/local/upsmon $(CURDIR)/debian/tmp/sbin/upsmon
	install -m 0755 debian/local/upsd $(CURDIR)/debian/tmp/sbin/upsd

	# Add a symlink to mask the LSB initscript
	ln -s nut-monitor.service $(CURDIR)/debian/tmp/lib/systemd/system/nut-client.service
	ln -s /dev/null $(CURDIR)/debian/tmp/lib/systemd/system/ups-monitor.service

	# install apport hooks
	install -D -m 644 debian/source_nut.py $(CURDIR)/debian/tmp/usr/share/apport/package-hooks/source_nut-server.py
	install -D -m 644 debian/source_nut.py $(CURDIR)/debian/tmp/usr/share/apport/package-hooks/source_nut-client.py

override_dh_auto_install-indep:
ifeq ($(filter nodoc,$(DEB_BUILD_PROFILES)),)
	#install documentation
	mkdir -p $(CURDIR)/debian/tmp/usr/share/doc/nut-doc/pdf
	mkdir -p $(CURDIR)/debian/tmp/usr/share/doc/nut-doc/html
	cp $(CURDIR)/docs/*.pdf $(CURDIR)/debian/tmp/usr/share/doc/nut-doc/pdf
	cp $(CURDIR)/docs/FAQ.html $(CURDIR)/debian/tmp/usr/share/doc/nut-doc/html

	for f in $(CURDIR)/docs/*.chunked/; do \
		nf=`basename $${f} | sed 's/\(.*\)\.chunked/\1/'`; \
		cp -a $${f} $(CURDIR)/debian/tmp/usr/share/doc/nut-doc/html/$${nf}/; \
	done
endif

	# install apport hooks
	install -D -m 644 debian/source_nut.py $(CURDIR)/debian/tmp/usr/share/apport/package-hooks/source_nut.py

override_dh_installinit:
	dh_installinit --error-handler=handle_start_failure

override_dh_compress:
	dh_compress -X.pdf

override_dh_installdocs:
	dh_installdocs -A ./README ./NEWS ./TODO ./AUTHORS

override_dh_installchangelogs:
# dh_installchangelogs automagically rename history.txt to changelog, let's try
# to avoid that
	dh_installchangelogs -pnut -Xhistory.txt
	dh_installchangelogs --remaining-packages
